using System;
using System.Collections.Generic;


namespace exercise28
{
    class Student : Person
    {
        int StudentId;

        public Student(string _name, int _age, int _studentId) : base (_name, _age)
        {
            StudentId = _studentId;

        }

       public void ListAllPapers(List<string> papers)
       {
          var output = string.Join(", ", papers);

          Console.WriteLine($"I am taking these papers: ");
          Console.WriteLine(output);
       }

       public void FavPaper(string favpaper)
       {
          Console.WriteLine($"My favourite paper is {favpaper}");

       }
       }
    }