namespace exercise28
{
    partial class Program
    {
        class Teacher : Person
        {
            int TeacherId;
            string Paper;
            public Teacher(string _name, int _age, string _paper, int _teacherid) : base(_name, _age)
            {
                TeacherId = _teacherid;
                Paper = _paper;
            }
        }
    }
}
