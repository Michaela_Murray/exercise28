using System;


namespace exercise28
{
   class Person
    {
        protected string Name;
        protected int Age;

        public Person() {}

        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }
        public void SayHello()
        {
            Console.WriteLine($"Hello There!");
            Console.WriteLine($"My name is {Name} and I am {Age}.");
        }
    }
}
