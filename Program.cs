﻿using System.Collections.Generic;
namespace exercise28
{
    partial class Program
    {
        static List<string> papers = new List<string> {"COMP5004", "COMP5008", "INFT5001", "COMP5002"};
        static void Main(string[] args)
        {
           var s1 = new Student("Natalia", 24, 27011537);
           s1.SayHello();
           s1.ListAllPapers(papers);
           s1.FavPaper(papers[3]);
           
           

           var s2 = new Student("Felicia", 32, 8700667);
           s2.SayHello();
           s2.ListAllPapers(papers);
           s2.FavPaper(papers[2]);
           
        }
    }
}
